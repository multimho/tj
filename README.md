# tj - Trusted Journalists Client

Client Java code snippet for trustedjournalists.org

## Trusted Journalists

Trusted journalists is a project to promote 'ad honimen' journalism. The idea
being that all journalists have a bias. So the trick is to find the journalists 
that have your bias and a journalist saying exactly the opposite.

## The concept

The solution consists of two things. A piece of javascript, that loads onto a
page (this client repository). Either with a plugin or via a snippet placed by
the publisher. 

This piece of code gathers special tags on the website to reassemble a plain
text version of the article. This plain text is then used to verify the
signature of the journalist. His or her public key is uploaded to 
trustedjournalists.org or a link is given to his/her own site. If the signature
is valid, a nice green flag is shown that this is an authentic article of the
journalist.

The javascript snippet also loads a picture, name, and remark of the author and
a link to his personal website or trustedjournalists.org personal page. There
the reader can view all (?) the articles the journalist has written. This way
the reader can assess the tone and or bias of the author.